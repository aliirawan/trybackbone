-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
    -- Host: localhost
-- Generation Time: Dec 13, 2013 at 09:29 AM
-- Server version: 5.6.12
-- PHP Version: 5.5.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
    -- Database: `trybackbone`
--
    CREATE DATABASE IF NOT EXISTS `trybackbone` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `trybackbone`;

-- --------------------------------------------------------

    --
        -- Table structure for table `contacts`
--

    CREATE TABLE IF NOT EXISTS `contacts` (
`id` bigint(20) NOT NULL AUTO_INCREMENT,
`name` varchar(50) NOT NULL,
`phone` varchar(50) NOT NULL,
`email` varchar(100) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
    -- Dumping data for table `contacts`
--

    INSERT INTO `contacts` (`id`, `name`, `phone`, `email`) VALUES
(1, 'Ali Irawan', '081287736665', 'boylevantz@gmail.com'),
    (2, 'John Smith', '081277847744', 'john.smith@gmail.com');
