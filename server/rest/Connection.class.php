<?php
class Connection {
    private $conn;

    protected $host;
    protected $db_name;
    protected $user;
    protected $pass;

    public function __construct(){
        $handle = @fopen("database.config", "r");
        if ($handle) {
            while (($buffer = fgets($handle, 4096)) !== false) {
                $array = explode('=', $buffer );
                $array[1] = substr($array[1],0,strlen($array[1])-1);
                switch($array[0]){
                    case 'host': $this->host = $array[1]; break;
                    case 'db_name': $this->db_name = $array[1]; break;
                    case 'user': $this->user = $array[1]; break;
                    case 'pass': $this->pass = $array[1]; break;
                }
            }
            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail\n";
            }
            fclose($handle);
        }
    }

    public function open(){
        $this->conn = mysql_connect($this->host, $this->user, $this->pass) or die(mysql_error());
        mysql_select_db($this->db_name) or die(mysql_error());
    }
    public function query($sql){
        return mysql_query($sql, $this->conn);
    }
    public function close(){
        mysql_close($this->conn);
    }
}
?>