define([
    'jquery',
    'backbone',
    'underscore',
    'models/contact',
    'collections/contacts',
    'text!templates/main.html'],
    function($, Backbone, _, ContactModel, ContactCollection, template){
        var View = Backbone.View.extend({
            el: '#content',
            initialize: function(){
                _.bindAll(this, "loadSuccess", "loadError");

                this.contacts = new ContactCollection();
                this.listenTo(this.contacts,'request', this.loadRequest);

                this.template = _.template( template, { model: this.contacts.toJSON() } );
            },
            loadData: function(){
                this.contacts.fetch({
                    success: this.loadSuccess,
                    error: this.loadError
                });
            },
            render: function(){
                console.log('Render');
                $(this.el).html( this.template );
            },
            loadRequest: function(collection,response,options){
                $(this.el).html('Loading...');
            },
            loadSuccess: function(collection,response,options){
                console.log('Contacts load success');
                var data = response;
                this.template = _.template( template, { model: data } );
                $(this.el).html( this.template );
            },
            loadError: function(collection,response,options){
                console.log('Contacts load error');
                $(this.el).html('Could not load data');
            }
        });

        return new View();
    });