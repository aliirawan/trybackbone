define([
    'jquery',
    'backbone',
    'underscore',
    'models/contact',
    'text!templates/add.html'],
    function($, Backbone, _, model, template){
        var View = Backbone.View.extend({
            el: '#content',
            initialize: function(){
                this.template = _.template( template, {} );
            },
            render: function(){
                console.log('Add view render');
                $(this.el).html( this.template );
            },
            events: {
                "click input[id=btnSave]": "doSave"
            },
            doSave: function(event){
                // Call ajax POST
                console.log('Do save()');

            }
        });

        return new View();
    });