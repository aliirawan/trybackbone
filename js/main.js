//  Author: Ali Irawan <boylevantz@gmail.com>
require.config({
        paths: {
            jquery: 'libs/jquery-min',
            underscore: 'libs/underscore-min',
            backbone: 'libs/backbone-min'
        },
        shim: {
            backbone: {
                deps: ['jquery','underscore'],
                exports: 'Backbone'
            },
            underscore: {
                exports: '_'
            }
        }
});

require(['underscore','backbone','app'],function(_, Backbone, App){
    console.log('____main.js');
    App.initialize();
});