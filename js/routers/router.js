define([
    'jquery',
    'underscore',
    'backbone',
    'views/list',
    'views/add'
], function($, _, Backbone, listView, addView) {

    console.log('____router.js');

    var AppRouter = Backbone.Router.extend({
        initialize: function(){

            this.listView = listView;
            this.addView = addView;
            Backbone.history.start();
        },
        routes: {
            '': 'home',
            'add': 'add'
        },
        'home': function(){
            this.listView.loadData();
            this.listView.render();
        },
        'add': function(){
            this.addView.render();
        }
    });
    return AppRouter;
});
